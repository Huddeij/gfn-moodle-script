"""Grade Calculator class"""
from gfn_script.classes.spider import Spider
from gfn_script.utils import grades


class Grade(Spider):
    """Grade calculator class (inherit of Spider)"""
    
    def navigate_to_grades(self) -> None:
        """Readout of grades from 'Bewertungen' page"""
        self.login()

        # navigate to 'Bewertungen'
        self.page.locator("#action-menu-toggle-1").click()
        self.page.wait_for_load_state("load")
        self.page.locator("a.dropdown-item:nth-child(5)").click()
        self.page.wait_for_load_state("load")

    def calc_grades(self):
        """Grade Point Average Calculator"""
        self.navigate_to_grades()

        # table readout
        table = self.page.locator("#overview-grade")
        row = table.locator("tr")
        column = row.locator("td.c1").all_inner_texts()
        self.logout()

        # Sanitize raw data, calculate the average percentage from it,
        # printing out the result.
        grade_avg = grades.grade_avg(column)
        print(f"\rDein Notendurchschnitt ist {grade_avg:.2f}%.")
        print(f"Deine Note ist eine {grades.assign_grade(grade_avg)}!")
