"""Timer Switch class"""
from datetime import timedelta as td
from sys import exit
from time import sleep

from playwright.sync_api import TimeoutError as PlaywrightTimeoutError

from gfn_script.classes.spider import Spider
from gfn_script.utils import timings


class Timer(Spider):
    """Timing switch class (inherit of Spider)"""

    def switch(self, time_of_day_switch):
        """
        Waiting for time of day and
        flipping the time recording switch accordingly.
        """
        try:
            self.page.locator(time_of_day_switch).click()
        except PlaywrightTimeoutError as pte:
            print("\r Button konnte nicht gefunden werden!      ")
            print(pte)
            self.logout()
            self.browser.close()
            exit()
        else:
            print("\r Erfolgreich Zeiterfassung umgestellt      ", end="")
            sleep(2)
            self.page.wait_for_load_state("load")
            self.logout()

    def until_lessons_start(self):
        """Checking time of day, starting the right switch accordingly"""
        early = td(hours=8, minutes=29, seconds=0)

        # time recording buttons on website
        early_switch = "input.btn"
        late_switch = "button.btn-primary:nth-child(1)"

        # Timer until school day start
        while timings.now() < early:
            timings.until_start()  # starts countdown until beginning of school day
            print("\r Stelle Zeiterfassung um                  ", end="")
            self.login()
            self.page.get_by_label("Im Homeoffice").check()
            self.switch(early_switch)

        # time until end of school day
        while early < timings.now():
            timings.until_end()  # starts countdown
            self.login()
            self.switch(late_switch)
            print("\r Es ist 16:30 Uhr. Schönen Feierabend      ")
            exit()
