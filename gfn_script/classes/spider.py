"""Webcrawler class"""
from playwright.sync_api import sync_playwright


class Spider:
    """Timing switch class (inherit of Spider)"""
    
    def __init__(self, creds: list, head: bool, path: str = "."):
        self.username = creds[0]
        self.password = creds[1]
        self.path = path
        self.head = head

        self.url = "https://lernplattform.gfn.de/login/index.php"

        playwright = sync_playwright().start()
        self.browser = playwright.chromium.launch(headless=head)
        self.page = self.browser.new_page(
            user_agent=(
                "Mozilla/5.0 (Windows NT 10.0; Win64; x64)"
                "AppleWebKit/537.36 (KHTML, like Gecko)"
                "Chrome/83.0.4103.116 Safari/537.36"
            )
        )
        self.page.set_default_timeout(100000)

    def login(self):
        """Logs user into GFN Moodle Platform"""
        self.page.goto(self.url)
        # Login Procedure
        self.page.wait_for_load_state("load")  # wait for the site to load

        # fill out credentials
        self.page.locator("#username").fill(self.username)
        self.page.locator("#password").fill(self.password)
        self.page.locator("#loginbtn").click()  # clicks login button
        self.page.wait_for_load_state("load")

    def logout(self):
        """Logs user out of GFN moodle Platform"""
        self.page.locator("#action-menu-toggle-1").click()
        self.page.wait_for_load_state("load")
        self.page.locator("#actionmenuaction-7").click()
        self.page.wait_for_load_state("load")
        self.page.close()
        self.browser.close()
