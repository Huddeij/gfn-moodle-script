"""Remote Downloader Class"""

from sys import exit

from playwright.sync_api import TimeoutError as PlaywrightTimeoutError

from gfn_script.classes.spider import Spider


class Remote(Spider):
    """Timing switch class (inherit of Spider)"""
    
    def navigate(self) -> list[str]:
        """Navigate to remote lab site"""
        try:
            # open navigation bar
            self.page.locator("#action-menu-toggle-1").click()
        except PlaywrightTimeoutError as pte:
            print("Button konnte nicht gefunden werden")
            print(pte)
            exit()
        else:
            self.page.wait_for_load_state("load")

        # click on profile
        self.page.locator('//*[@id="action-menu-1-menu"]/a[3]').click()
        self.page.wait_for_load_state("load")

        # click on details
        self.page.locator('//*[@id="details-tab"]').click()

        # click on “Meine Remotelabs“
        self.page.locator(
            '//*[@id="details"]/div/div/div/section[4]/div/ul/li[5]/span/a'
        ).click()
        self.page.wait_for_load_state("load")

        # get all information about the remotelab access
        return (
            self.page.locator("#region-main>div>div>div>table>tbody>tr")
            .all_inner_texts()[0]
            .split("\t")
        )

    def remotelab(self):
        """Remotelab Access Downloader: Navigate"""
        self.login()
        self.navigate()

        # download the rdp file and save to desktop
        with self.page.expect_download() as download_info:
            self.page.locator("text=Starten").click()
        download = download_info.value
        download.save_as(f"{self.path}/remotelab.rdp")
        print(" Datei gespeichert!")
