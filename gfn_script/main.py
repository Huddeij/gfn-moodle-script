"""
Multifunctional GFN Crawling Script
Author: Huddeij
Mail: huddeij@tuta.io
"""

import argparse
import sys
from os import path
from time import sleep

from gfn_script.classes import grade, remote, timer
from gfn_script.utils import login_encryption, rest_of_time

MODULES = {
    "time": "Automatischer Zeiterfassungsschalter",
    "grade": "Notenrechner",
    "remote": "Downloader des Remote-Lab Zugangs",
    "rest": "Restzeit-Rechner der Umschulung",
}


def help_msg() -> None:
    """Help message function"""
    m_help = {
        "time": "Tageszeitabhängiges Starten und Stoppen der Zeiterfassung.",
        "grade": "Notendurchschnittsberechnung, von deiner GFN Bewertungsseite",
        "remote": (
            "Download der Remote-Desktop-Datei in vorausgewählten "
            "(--d /Pfad/zum/Ordner), oder als Standard in diesen Ordner"
        ),
        "rest": "Berechnung der Restzeit der Umschulung und dem Praktikumsbeginn.",
    }

    list_string = ["Argument", "Modulname", "Beschreibung"]

    print(
        "   Skript zur Automatisierung der GFN Moodle Platform."
        "Mit Argumenten\n   können Module gestartet werden."
        "Argumente werden hinter das Skript geschrieben.\n"
    )

    # iterate and print through titles and MODULES in form of a table.
    print(f" {list_string[0]:<12} {list_string[1]:<38} {list_string[2]:<1}")
    for key, module in MODULES.items():
        arguments, name, desc = [key, module, m_help[key]]
        print(f"  {arguments:<7} {name:<38} {desc:<157}")

    print('\n  Beispiel: "python main.py remote --d C:/Users/student/Desktop"\n')


def logo():
    """Prints logo"""
    print("  __  __     __  __     _____     _____     ______     __       __")
    print(
        " /\\ \\_\\ \\   /\\ \\/\\ \\   /\\  __-.  /\\  __-."
        "  /\\  ___\\   /\\"
        " \\     /\\ \\ "
    )
    print(
        " \\ \\  __ \\  \\ \\ \\_\\ \\  \\ \\ \\/\\ \\ \\ \\"
        " \\/\\ \\ \\ \\  _"
        "_\\   \\ \\ \\   _\\_\\ \\ "
    )
    print(
        "  \\ \\_\\ \\_\\  \\ \\_____\\  \\ \\____-  \\ \\__"
        "__-  \\ \\_____\\"
        "  \\ \\_\\ /\\_____\\ "
    )
    print(
        "   \\/_/\\/_/   \\/_____/   \\/____/   \\/____/   \\/_____/   \\/_/ \\/_____/"
    )


def login_credentials() -> list[str]:
    """
    Checks if credentials (username, password) present in 'login.txt'-file
    If not, asks user for a username and password and saves them together with
    a key file encrypted in the script folder. The key file must be kept in the
    folder, otherwise the login.txt file can never be decrypted again
    """
    if not path.exists("gfn_script/login.txt"):
        print("Konnte keine Anmeldeinformationen finden \n")
        login_encryption.save_credentials()
    return login_encryption.decrypt()


def remotelab_parser(param) -> None:
    """Remotelab save folder function → sends folder path to Remote class"""
    if param.destination:
        print(f"\r  Dateien werden in {param.destination} gespeichert.", end="")
        remote.Remote(login_credentials(), param.head, param.destination).remotelab()
    print(" Datei wird im Skript-Ordner gespeichert\n")
    sleep(1.5)
    remote.Remote(login_credentials(), param.head).remotelab()


def main(param) -> None:
    """Main function → operates given parameter and starts according functions"""
    credentials = login_credentials()

    if param.module == list(MODULES.keys())[0]:
        # 'Time Tracking Auto-Switcher'
        print("\r\tZeiterfassungsschalter wird gestartet")
        # Starts Timer module
        timer.Timer(credentials, param.head).until_lessons_start()

    elif param.module == list(MODULES.keys())[1]:
        # 'Grade Calculator'
        print("\r\tNotenrechner wird gestartet")
        # Starts Grade Module
        grade.Grade(credentials, param.head).calc_grades()

    elif param.module == list(MODULES.keys())[2]:
        # 'Download of Remote-Lab Access'
        print("\r\tRemote Lab Downloader wird gestartet")
        # Calls Remotelab Function to evaluate the file saving path
        remotelab_parser(param)

    elif param.module == list(MODULES.keys())[3]:
        # Calls the Rest of Time Function
        rest_of_time.rest_of_time()

    else:
        # If an incorrect input given
        print("Unerlaubter Parameter benutzt. Programm wird beendet")


if __name__ == "__main__":
    print("\n\t GFN SCRIPT COLLECTION - by")
    logo()
    print(70 * "=", "\n")

    if len(sys.argv) == 1 or sys.argv[1] in ["-h", "--help"]:
        help_msg()
        sys.exit()

    parser = argparse.ArgumentParser()
    parser.add_argument(
        "module", type=str, choices=MODULES.keys(), help="Die Module des Scripts"
    )

    parser.add_argument(
        "--d",
        type=str,
        dest="destination",
        help="Speicherort für die Remotelab Dateien",
    )

    parser.add_argument(
        "--head",
        action="store_false",
        dest="head",
        help="Sichtbarer Browser. Nur zum Debuggen!",
    )

    args = parser.parse_args()
    main(args)
