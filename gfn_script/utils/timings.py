"""Collection of timing and day of time functions"""

from datetime import datetime as dt
from datetime import timedelta as td
from time import sleep


def now():
    """Look on the clock"""
    return td(
        hours=dt.now().time().hour,
        minutes=dt.now().time().minute,
        seconds=dt.now().time().second,
    )


def until_start():
    """Countdown to begin of school day"""
    early = td(hours=8, minutes=29, seconds=0)

    while early >= now():
        print("\r      ", early - now(), "bis Schulstart         ", end="")
        sleep(1)


def until_end():
    """Countdown top end of school day"""
    late = td(hours=16, minutes=31, seconds=0)
    while late >= now():
        print("\r      ", late - now(), "bis zum Feierabend       ", end="")
        sleep(1)
