"""Grade calculator: input percentage -> get german style grade"""


def grade_avg(column) -> float:
    """Return the mean of percentages from GFN's raw data"""
    # Sanitize raw data down to the percentages
    grade_percentages = [
        i[-9:-3].replace(",", ".").replace("(", "") for i in column if i != "-"
    ]
    grade = [float(i) for i in grade_percentages if i != ""]

    # return mean of percentages of all courses
    return sum(grade) / len(grade)


def assign_grade(grade: float):
    """Calculates grade from the readout percentages"""
    if grade > 91.0:
        return 1
    if grade > 80.0:
        return 2
    if grade > 66.0:
        return 3
    return 4 if grade > 49.0 else "Nicht bestanden"
