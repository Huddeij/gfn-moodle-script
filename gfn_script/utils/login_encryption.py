"""De- and Encrypt login.txt"""

import os
from sys import exit


def _encrypt():
    """
    Opens 'login.txt'-file in cleartext, creates a key-file, encrypts
    with it the credentials and stores them to the same file ('login.txt')
    """
    try:
        # opens cleartext login.txt
        with open("gfn_script/login.txt", "rb") as file:
            to_encrypt = file.read()
    except FileNotFoundError:
        print("login.txt konnte nicht gefunden werden")
        exit()

    # creates encryption key
    size = len(to_encrypt)
    key = os.urandom(size)

    try:
        # stores en/decryption key to login.txt.key
        with open("gfn_script/login.txt.key", "wb") as key_out:
            key_out.write(key)
    except FileNotFoundError:
        print("login.txt.key konnte nicht gefunden werden")
        exit()

    # encryption process
    encrypted = bytes(a ^ b for (a, b) in zip(to_encrypt, key))

    # stores encrypted data back to login.txt
    with open("gfn_script/login.txt", "wb") as encrypted_out:
        encrypted_out.write(encrypted)


def decrypt():
    """
    Opens encrypted 'login.txt'-file, decrypts it with help from
    the key file ('login.txt.key') and returns the decrypted credentials.
    """
    try:
        # opens encrypted login.txt file, stores in file variable
        with open("gfn_script/login.txt", "rb") as file:
            encrypted_file = file.read()

        # opens key file for decryption, stores in key variable
        with open("gfn_script/login.txt.key", "rb") as key_file:
            crypt_key = key_file.read()

    except FileNotFoundError:
        print("login.txt oder login.txt.key konnte nicht gefunden werden")
        exit()

    # decrypts file with key
    decrypted = bytes(a ^ b for (a, b) in zip(encrypted_file, crypt_key))

    # returns decrypted credentials for login
    return str(decrypted)[2:-1].split(":")


def save_credentials():
    """
    Asks user for credentials, saves them to login.txt
    and calls encrypt function.
    """
    with open("gfn_script/login.txt", "w", encoding="UTF-8") as file:
        file.write(input(" Bitte deinen Usernamen eingeben: "))
        file.write(":")
        file.write(input(" Bitte dein Password eingeben: "))
    _encrypt()
