"""Rest of Time calculator"""

import locale
from datetime import datetime as dt


def rest_of_time() -> None:
    """'Restzeit-Rechner der Umschulung'"""
    def calc_times(begin_func, today_func, eos_func, begin_is) -> list[int]:
        return [
            100 - ((eos_func - today_func) / (eos_func - begin_func) * 100),
            (eos_func - today_func) / (eos_func - begin_func) * 100,
            100 - ((begin_is - today_func) / (today_func - begin_func) * 100),
        ]

    begin = dt(2022, 1, 24)
    today = dt.now()
    eos = dt(2024, 1, 23)
    begin_internship = dt(2023, 4, 24)
    locale.setlocale(locale.LC_ALL, "de_DE")
    dates_of_function = calc_times(begin, today, eos, begin_internship)

    print(f" Heute ist {today.strftime('%d. %B %Y')}.")
    print(
        " Die Umschulung hat am"
        f" {begin.strftime('%d. %B %Y')} begonnen,"
        f" vor {(today - begin).days} Tagen."
    )
    print(
        f" Es liegen insgesamt {dates_of_function[0]:.1f}"
        f"% hinter und noch {(eos - today).days} "
        f"Tage vor uns, also {dates_of_function[1]:.1f}%."
    )
    print(
        " Wir haben bis zum Praktikum noch"
        f" {(begin_internship - today).days}"
        f" Tage, also {dates_of_function[0]:.1f}%."
    )
