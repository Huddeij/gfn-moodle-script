<table>
  <tr>
    <td><img src="https://i.ibb.co/9rn6ydf/logo-light.png" style="float:left;" alt="Logo"></img></td>
    <td><h1>GFN Script</h1></td>
  </tr>
</table>

<h3>Comprehensive Moodle script for
<a href="https://lernplattform.gfn.de" target="_blank">GFN</a> IT school.</h3>

[![DeepSource](https://deepsource.io/gl/Huddeij/gfn_script.svg/?label=active+issues&show_trend=true&token=-bBfStvCE9sp2N9r6-X6uTBg)](https://deepsource.io/gl/Huddeij/gfn_script/?ref=repository-badge)
[![DeepSource](https://deepsource.io/gl/Huddeij/gfn_script.svg/?label=resolved+issues&show_trend=true&token=-bBfStvCE9sp2N9r6-X6uTBg)](https://deepsource.io/gl/Huddeij/gfn_script/?ref=repository-badge)

4 Modules implemented right now:
1. <b>[Automated time recording switch depending on time of day](#timer)</b>
2. <b>[Grade calculator](#grade)</b>
3. <b>[Remotelab (rdp) file downloader](#remote)</b>
4. <b>[Rest of time calculator of retraining and internship](#remain)</b>

### <a name="general"></a>General
- The script depends on the module <a href="https://playwright.dev" target="_blank">Playwright</a>.
  More details [here](https://playwright.dev/python/docs/intro).
  The script uses the Chromium browser module.

### <a name="installation"></a>Installation
- Recommended usage of the script in a virtual environment (venv). To set one up,
  navigate to a folder of your choice, write <code>python -m venv gfn</code> in a
  command prompt of your choice
- Navigate into the newly created folder with <code>cd gfn</code> and start it
with <code>gfn\Script\activate</code>
- Clone the repo by writing <code>git clone https://gitlab.com/Huddeij/gfn_script.git </code>
- Navigate into it via <code>cd gfn_script</code> and install the dependencies
  (<code>pip install .</code>)
- Next you need to install Playwright's dependencies with <code>playwright install</code>

### <a name="usage"></a>Usage
- After successful installation, start the script's help function with
  <code>python -m gfn_script.main -h</code>.
- On the first run, the script asks for user's login credentials and saves them encrypted
  to 'login.txt'-file together with a keyfile ('login.txt.key') for later use in script's
  root folder. Please keep in mind, <b>loosing or damaging the keyfile will render
  the decryption process impossible!</b> You'll have to delete login.txt and repeat the process.
- Start the script with <code>python -m gfn_script.main</code> with the arguments
  respectively to the order of the module<br />
  (Example: <code>python -m gfn_script.main time</code> for the time recording switch)
- If something goes wrong, you can follow the browser's navigation process of the script
  by starting it "<i>headful</i>" by putting <code>--head</code> behind the other parameters. <br>
  (Example: <code>python -m gfn_scrip.main remote --head</code>)

## <a name="timer"></a><i>"time"</i> - Automated Time Recording Switch
Before 8:30 (begin of school day) or after 16:30 (end of school day) the script pushes the time
recording button accordingly.
During school day, it prints out how long the day of school will last in hours, minutes and
seconds to terminal.

## <a name="grade"></a><i>"grade"</i> - Grade Calculator
The Script scrapes for your learning target percentages and calculates the mean of them.
Corresponding to the official 'IHK Notenschlüssel' your grade gets printed out as well.

## <a name="remote"></a><i>"remote"</i> - Remotelab rdp File Downloader
An automated Remotelab rdp-file Downloader.
Downloaded either by default into the script's folder or per argument to a folder of your choice.
(Example: <code>python -m gfn_script.main remote --d C:\files</code>)

## <a name="remain"></a><i>"rest"</i> - Remaining time calculator of the retraining
Checks today's date and prints out to the terminal the time accomplished in school by the user
to date, and the time ahead to graduation and to the start of the internship in days and percent.

### <a name="bugs"></a>KNOWN BUGS
- If Moodle's right sidebar is collapsed
  (and Moodle remembers this across multiple devices unfortunately!),
  the modules won't work. Please keep your sidebar opened to guarantee proper functionality.
- If you're logged into the platform on too many devices at the same time, login gets
  prohibited with an "already logged in" error page.
  Just don't be logged into the GFN platform on every possible device you can find.

### <a name="future"></a>Future Plans / Feature Implementation
- None
- If you wish to get additional features, write <a href="mailto:huddeij@tuta.io">me an email</a>.
